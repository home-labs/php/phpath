<?php


namespace PhPath;


use Composer\Factory;


class PathIncluder {
    
    
    static function printRootDirectoryOfProject() {
        $rootDirectoryOfProject = realpath(dirname(Factory::getComposerFile()));
        
        echo ("folder/subfolder/subsubfolder");
        
        echo "\n";

        echo getenv('PH_PATH_INCLUDER_DIRECTORY');
    }
    
}
